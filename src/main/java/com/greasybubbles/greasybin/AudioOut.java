package com.greasybubbles.greasybin;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

class AudioOut {

    private final SourceDataLine line;
    private final int bytesPerSample;
    private final int nrChannels;

    public AudioOut(int nrChannels, int sampleRate, int bytesPerSample, int bufferLengthMillis) {

        this.bytesPerSample = bytesPerSample;
        this.nrChannels = nrChannels;

        int bufferLengthBytes = nrChannels * bytesPerSample * sampleRate * bufferLengthMillis / 1000;

        AudioFormat audioFormat = new AudioFormat((float)sampleRate, bytesPerSample*8, nrChannels, true, false);
        SourceDataLine.Info info = new SourceDataLine.Info(DataLine.class, audioFormat);

        try {
            line = (SourceDataLine)AudioSystem.getLine(info);
            line.open(audioFormat, bufferLengthBytes);
        } catch (LineUnavailableException e) {
            throw new RuntimeException("Unable to open audio line.", e);
        }
        assert (line.getBufferSize() == bufferLengthBytes);
        line.start();
    }

    //return whether there is enough space in the buffer to send over a specified amount of samples
    public boolean readyFor(int nSamples) {
        return (line.available() > nSamples * bytesPerSample * nrChannels);
    }


    public void play(int[] left, int[] right) {
        assert(left.length == right.length);
        byte[] byteStream = new byte[(left.length + right.length) * bytesPerSample];
        for (int i = 0; i < left.length; i++) {
            int samplel = left[i];
            int sampler = right[i];
            samplel = Math.max(-32768, Math.min(32767, samplel));
            sampler = Math.max(-32768, Math.min(32767, sampler));
            byte[] bytePairL = shortToLoHiBytes(samplel);
            byte[] bytePairR = shortToLoHiBytes(sampler);
            byteStream[i*4+0] = bytePairL[0];
            byteStream[i*4+1] = bytePairL[1];
            byteStream[i*4+2] = bytePairR[0];
            byteStream[i*4+3] = bytePairR[1];
        }
        line.write(byteStream, 0, byteStream.length);
    }

    void play(int[] samples) {
        byte[] byteStream = new byte[samples.length*2];
        for (int i = 0; i < samples.length; i++) {
            int sample = samples[i];
            sample = Math.max(-32768, Math.min(32767, sample));
            byte[] bytePair = shortToLoHiBytes(sample);
            byteStream[i*2] = bytePair[0];
            byteStream[i*2+1] = bytePair[1];
        }
        line.write(byteStream, 0, byteStream.length);
    }

    private static byte[] shortToLoHiBytes(int sample) {
        byte high = (byte) ((sample >> 8) & 0xFF);
        byte low = (byte) (sample & 0xFF);
        byte[] returnValue = new byte[2];
        returnValue[0] = high;
        returnValue[1] = low; //big endian? little endian? check!
        return returnValue;
    }

    public void endAudio() {
        line.drain();
        line.stop();
    }
}