package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case class Bytes(count: Int) extends Format[Seq[Byte]] {
  override def size: Int = count
  override def parse(data: Stream[Byte]): Vector[Byte] = data.take(count).toVector
}
