package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case class Composite[A](parts: List[Section], assemble: Map[String,Any] => A) extends Format[A] {
  private val seqFormat = Sequence(parts)
  override def size: Int = seqFormat.size
  override def parse(data: Stream[Byte]): A = assemble(seqFormat.parse(data))
}
