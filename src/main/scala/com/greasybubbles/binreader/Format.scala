package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
trait Format[A] {
  def size: Int
  def parse(data: Stream[Byte]): A
}
