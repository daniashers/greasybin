package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case class Repeat[A](what: Format[A], count: Int) extends Format[Vector[A]] {
  override def size: Int = what.size * count
  override def parse(data: Stream[Byte]): Vector[A] = {
    @annotation.tailrec
    def go(accu: Vector[A], s: Stream[Byte]): Vector[A] = {
      if (accu.size == count) accu
      else {
        val newElement = what.parse(s)
        go(accu :+ newElement, s.drop(what.size))
      }
    }
    go(Vector.empty[A],data)
  }
}
