package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case class Section(name: String, format: Format[_])
