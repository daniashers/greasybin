package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case class Sequence(parts: List[Section]) extends Format[Map[String,Any]] {
  override def size: Int = parts.foldLeft(0)(_ + _.format.size)

  override def parse(data: Stream[Byte]): Map[String,Any] = {
    @annotation.tailrec
    def go(accu: Map[String,Any], remainingSections: List[Section], s: Stream[Byte]): Map[String,Any] = {
      if (remainingSections.isEmpty) accu
      else {
        val format = remainingSections.head.format
        val newValue = format.parse(s)
        val newAccu = accu + (remainingSections.head.name -> newValue)
        go(newAccu, remainingSections.tail, s.drop(format.size))
      }
    }
    go(Map.empty[String,Any],parts,data)
  }
}
