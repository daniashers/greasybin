package com.greasybubbles.binreader

case class SequenceOfSameType[A](parts: List[Format[A]]) extends Format[Vector[A]] {
  override def size: Int = parts.foldLeft(0)(_ + _.size)

  override def parse(data: Stream[Byte]): Vector[A] = {
    @annotation.tailrec
    def go(accu: Vector[A], remaining: List[Format[A]], s: Stream[Byte]): Vector[A] = {
      if (remaining.isEmpty) accu
      else {
        val format = remaining.head
        val newValue = format.parse(s)
        val newAccu = accu :+ newValue
        go(newAccu, remaining.tail, s.drop(format.size))
      }
    }
    go(Vector.empty[A],parts,data)
  }
}
