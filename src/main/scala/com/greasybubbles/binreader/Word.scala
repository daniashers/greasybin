package com.greasybubbles.binreader

/**
 * Created by dani on 03/10/2015.
 */
case object Word extends Format[Int] {
  override def size: Int = 2
  override def parse(data: Stream[Byte]): Int = toUnsigned(data.head)*256 + toUnsigned(data.drop(1).head)

  def toUnsigned(b: Byte): Int = if (b < 0) b+256 else b
}
