package com.greasybubbles.binreader.formats

import com.greasybubbles.binreader.Format

case object Byte extends Format[Int] {
  override def size: Int = 1
  override def parse(data: Stream[Byte]): Int = toUnsigned(data.head)

  def toUnsigned(b: Byte): Int = if (b < 0) b+256 else b
}
