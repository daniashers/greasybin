package com.greasybubbles.binreader.formats

import com.greasybubbles.binreader.Format

case class NullPaddedString(length: Int) extends Format[String] {
  override def size: Int = length
  override def parse(data: Stream[Byte]): String = data.take(length).takeWhile(_ != 0x00).foldLeft("")(_ + _.toChar)
}
