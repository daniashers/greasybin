package com.greasybubbles.greasybin

import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.LineUnavailableException
import javax.sound.sampled.SourceDataLine
import javax.sound.sampled.DataLine.Info

class AudioPort(nrChannels: Int, sampleRate: Int, bytesPerSample: Int, bufferLengthMillis: Int) {
  private var line: SourceDataLine = null

  val bufferLengthBytes: Int = nrChannels * bytesPerSample * sampleRate * bufferLengthMillis / 1000;

  val audioFormat: AudioFormat = new AudioFormat(sampleRate, bytesPerSample * 8, nrChannels, true, false);

  val info: Info = new Info(classOf[DataLine], audioFormat);

  try {
    line = AudioSystem.getLine(info).asInstanceOf[SourceDataLine];
    line.open(audioFormat, bufferLengthBytes);
  } catch {
    case e: LineUnavailableException => throw new RuntimeException("Unable to open audio line.", e);
  }
  assert (line.getBufferSize() == bufferLengthBytes);
  line.start();

  def playRaw(samples: Array[Byte]) {
    line.write(samples, 0, samples.length)
  }

  def readyFor(nrSamples: Int): Boolean = {
    (line.available > nrSamples * bytesPerSample * nrChannels)
  }
}
