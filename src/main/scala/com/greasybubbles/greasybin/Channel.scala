package com.greasybubbles.greasybin


case class Channel(
                      instrument: Instrument,
                      effect: Effect,
                      basePeriod: Int,
                      period: Int = 0,
                      volume: Int,
                      pan: Int = 0,
                      effectRegister: Int = 0,
                      slideSpeed: Int = 0,
                      vibratoWaveform: Waveform = Sine(),
                      vibratoAmplitude: Int = 0,
                      vibratoRate: Int = 0,
                      tremoloWaveform: Waveform = Sine(),
                      tremoloAmplitude: Int = 0,
                      tremoloRate: Int = 0,
                      sampleIndex: Int = 0, periodCounter: Int = 0) {

  def retune(nominal: Int): Int = Math.round(nominal * instrument.finetuneFactor).toInt

  def bendBySemitones(n: Double) = {
    def newPeriod = Math.round(basePeriod.toDouble / Math.pow(Music.Semitone, n)).toInt
    this.copy(period = newPeriod)
  }

  def resetSample: Channel = this.copy(sampleIndex = 0, periodCounter = 0)

  def applyEvent(e: Event, i: Seq[Instrument]): Channel = {
    val isSlideTo = e.effect match { //THIS IS A HORRID HACK TODO!
      case SlideTo(_) => true
      case VolumeSlideWithSlideTo(_) => true
      case _ => false
    }

    val x1 = if (e.note.sampleNr != 0) {
      // a new note on has occurred
      val instr = i(e.note.sampleNr-1)
      this.copy(instrument = instr, volume = instr.defaultVolume).resetSample.copy(effect = e.effect)
    } else {
      this.copy(effect = e.effect)
    }
    val x2 = if (e.note.period != 0) {
      // a new period has been specified
      x1.copy(basePeriod = x1.retune(e.note.period), period = if (isSlideTo) x1.period else x1.retune(e.note.period))
    } else {
      x1
    }
    x2.effect.init(x2)
  }

  def applyEffect: Channel = {
    effect.transform(this)
  }

  def next: Channel = {
    this.effect.transform(this)
  }
}

object Channel {
  val Initial = Channel(
    instrument = Instrument.NullInstrument,
    effect = NoEffect,
    basePeriod = 0,
    volume = 0,
    pan = 0,
    slideSpeed = 0,
    vibratoWaveform = Sine(true),
    vibratoAmplitude = 0,
    tremoloAmplitude = 0,
    vibratoRate = 0,
    tremoloWaveform = Sine(true)
    )
}