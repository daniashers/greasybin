package com.greasybubbles.greasybin

import com.greasybubbles.greasybin.usefulint._

abstract class Effect {
  def init(ch: Channel): Channel // do nothing by default
  def transform(ch: Channel): Channel
  def transform(seq: Sequencer): Sequencer
  def describe: String
}

abstract class SequenceEffect extends Effect {
  def init(ch: Channel): Channel = ch
  def transform(ch: Channel): Channel = ch
}

abstract class ChannelEffect extends Effect {
  def init(ch: Channel): Channel = ch
  def transform(seq: Sequencer): Sequencer = seq
}

abstract class HexEffect(val hex: Int) extends Effect

case class Unimplemented(nr: Int, param: Int) extends Effect {
  def init(ch: Channel): Channel = ch
  def transform(seq: Sequencer): Sequencer = seq
  def transform(ch: Channel): Channel = ch
  override def describe: String = "! UNIMPL %X%02X".format(nr,param)
}

// 0xy
case class Arpeggio(n1: Int, n2: Int) extends ChannelEffect {
  override def init(ch: Channel) = ch.copy(effectRegister = 0)
  override def transform(ch: Channel): Channel = {
    val arpPos = ch.effectRegister
    val interval = arpPos match {
      case 0 => 0
      case 1 => n1
      case 2 => n2
      case _ => 0
    }
    ch.bendBySemitones(interval).copy(effectRegister = (arpPos + 1) % 3)
  }

  override def describe: String = "ARPEGGIO %X%X".format(n1,n2)
}

// Axy
case class VolumeSlide(rate: Int) extends ChannelEffect {
  override def transform(ch: Channel): Channel = {
    ch.copy(volume = (ch.volume + rate).clamp(0, 64))
  }
  override def describe: String = "VOLUME %+02d".format(rate)
}

//1xx
//2xx
case class PitchSlide(rate: Int) extends ChannelEffect {
  override def transform(ch: Channel): Channel = {
    ch.copy(period = (ch.period + rate).clamp(113, 856))
  }
  override def describe: String = "PERIOD %+02d".format(rate)
}

//3xx
case class SlideTo(rate: Int) extends ChannelEffect {
  override def init(ch: Channel): Channel = {
    if (!(rate == 0)) ch.copy(slideSpeed = rate) else ch
  }
  override def transform(ch: Channel): Channel = {
    val target = ch.basePeriod
    if (ch.period < target) {
      ch.copy(period = (ch.period + ch.slideSpeed) min target)
    } else {
      ch.copy(period = (ch.period - ch.slideSpeed) max target)
    }
  }
  override def describe: String = "SLIDE TO %02X".format(rate)
}

//4xy
case class Vibrato(amplitude: Int, rate: Int) extends ChannelEffect {
  def useMemorizedValues = (amplitude == 0 && rate == 0)
  override def init(ch: Channel): Channel = {
    if (!useMemorizedValues) ch.copy(vibratoAmplitude = amplitude, vibratoRate = rate) else ch
  }
  override def transform(ch: Channel): Channel = {
    def vibratoRange = Math.round((Music.Semitone - 1) * ch.basePeriod * ch.vibratoAmplitude / 16.0).toInt // 'amplitude' 16ths of a semitone
    val periodModifier = Math.round(ch.vibratoWaveform.get(ch.effectRegister) * vibratoRange).toInt
    ch.copy(period = (ch.basePeriod + periodModifier), effectRegister = (ch.effectRegister + ch.vibratoRate) % 64)
  }
  override def describe: String = if (useMemorizedValues) "VIBRATO MEM" else "VIBRATO %X%X".format(amplitude,rate)
}

//5xy
case class VolumeSlideWithSlideTo(rate: Int) extends ChannelEffect  {
  val fx1 = VolumeSlide(rate)
  val fx2 = SlideTo(0)
  override def init(ch: Channel): Channel = {
    ch
  }
  override def transform(ch: Channel): Channel = {
    fx2.transform(fx1.transform(ch))
  }
  override def describe: String = "VOL+SLTO %2X".format(rate)
}

//6xy
case class VolumeSlideWithVibrato(rate: Int) extends ChannelEffect {
  val fx1 = VolumeSlide(rate)
  val fx2 = Vibrato(0,0)
  override def init(ch: Channel): Channel = {
    ch
  }
  override def transform(ch: Channel): Channel = {
    fx2.transform(fx1.transform(ch))
  }
  override def describe: String = "VOL+VIBR %+02d".format(rate)
}

//7xy
//case class Tremolo(amplitude: Int, rate: Int) extends ChannelEffect {
//  override def init(ch: Channel): Channel = {
//    if (!(amplitude == 0 && rate == 0)) ch.copy(tremoloAmplitude = amplitude, tremoloRate = rate) else ch
//    //TODO reset phase??
//  }
//  override def transform(ch: Channel): Channel = {
//    def tremoloRange = ch.tremoloAmplitude //TODO is this the actual range? Is there a scaling factor?
//    val volumeModifier = Math.round(ch.tremoloWaveform.get(ch.effectRegister) * tremoloRange).toInt
//    ch.copy(volume = (ch.volume + volumeModifier), effectRegister = (ch.effectRegister + ch.tremoloRate) % 64)
//  }
//} //TODO the algorithm is WRONG. There should be a 'base' volume that is the centre of the oscillation and doesn't change!

//case class SetSampleOffset(offset: Int) extends Effect          // 9

case class PositionJump(destination: Int) extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "SEQ JUMP %2X".format(destination)
} // b

//Cxx
case class SetVolume(vol: Int) extends ChannelEffect {
  override def init(ch: Channel): Channel = {
    ch.copy(volume = vol.clamp(0, 64))
  }
  override def transform(ch: Channel): Channel = ch
  override def describe: String = "SET VOL %2X".format(vol)
}

//Dxx
case class PatternBreak(resumePos: Int) extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "PTN BREAK %2X".format(resumePos)
}

//case class FinePitchSlide(amount: Int) extends Effect // E1x/E2x
//

//E4x
case class SetVibratoWaveform(waveform: Waveform) extends ChannelEffect {
  override def init(ch: Channel): Channel = ch.copy(vibratoWaveform = waveform)
  override def transform(ch: Channel): Channel = ch
  override def describe: String = "VIBRWAVE X"
}

//E60
case object MarkLoopStart extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "LOOP START"
}

//E6x
case class MarkLoopEnd(repeatCount: Int) extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "LOOP END %2X".format(repeatCount)
}

//E7x
case class SetTremoloWaveform(waveform: Waveform) extends ChannelEffect {
  override def init(ch: Channel): Channel = ch.copy(tremoloWaveform = waveform)
  override def transform(ch: Channel): Channel = ch
  override def describe: String = "TREMWAVE X"
}

///*     */ case class RetriggerSampleEvery(periodTicks: Int) extends Effect // E9x
//case class FineVolumeSlide(amount: Int) extends Effect // EAx/EBx
///* CUT */ case class CutSampleAfterXTicks(ticks: Int) extends Effect // ECx
///* DLY */ case class DelaySampleByTicks(ticks: Int) extends Effect // EDx

// Fxx if xx > 32
case class SetTicksPerDivision(value: Int) extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "SPEED TPD %2X".format(value)
}

// Fxx if xx <= 32
case class SetBeatsPerMinute(value: Int) extends SequenceEffect {
  override def transform(seq: Sequencer): Sequencer = seq //TODO
  override def describe: String = "SPEED BPM %2X".format(value)
}

case object NoEffect extends Effect {
  // do nothing
  override def init(ch: Channel) = ch
  override def transform(ch: Channel) = ch
  override def transform(seq: Sequencer) = seq
  override def describe: String = " ----- "
}

//WILL NOT IMPLEMENT:
//case class SetPan(pos: Int) extends Effect          // 8
//case class SetFilter(onOff: Boolean) extends Effect // E00/E01 ?? Unused ??
//case class DelayPatternByDivisions(divisions: Int) extends Effect // EEx
//case class SetGlissando(onOff: Boolean) extends Effect // E30/E31 WILL NOT IMPLEMENT
//case class InvertSampleLoop(speed: Int) extends Effect // EFx WILL NOT IMPLEMENT



object Effect {
  val waveforms: Seq[Waveform] = Seq(
    Sine(), SawDown(), Square(), Random(),
    Sine(false), SawDown(false), Square(false), Random(false))

  def decode(nr: Int, param: Int): Effect = nr match {
    case 0x0 => if (param==0) NoEffect else Arpeggio(param.highNybble, param.lowNybble)
    case 0x1 => PitchSlide(-param)
    case 0x2 => PitchSlide(+param)
    case 0x3 => SlideTo(param)
    case 0x4 => Vibrato(param.lowNybble, param.highNybble)
    case 0x5 => VolumeSlideWithSlideTo(+ param.highNybble - param.lowNybble)
    case 0x6 => VolumeSlideWithVibrato(+ param.highNybble - param.lowNybble)
//    case 0x7 => Tremolo(param.loNibble, param.hiNibble)
//    case class RetriggerSampleEvery(periodTicks: Int) extends Effect // E9x
//    case class SetSampleOffset(offset: Int) extends Effect          // 9
    case 0xa => VolumeSlide(+ param.highNybble - param.lowNybble)
    case 0xb => PositionJump(param)
    case 0xc => SetVolume(param)
    case 0xd => PatternBreak(param)
//    case class FinePitchSlide(amount: Int) extends Effect // E1x/E2x
    case 0xe if (param.highNybble == 4) => SetVibratoWaveform(waveforms(param.lowNybble))
//    case class SetFineTune(waveform: Waveform, retrigger: Boolean) extends Effect // E5x
    case 0xe if (param == 0x60) => MarkLoopStart
    case 0xe if (param >= 0x61 && param <= 0x6f) => MarkLoopEnd(param.lowNybble)
    case 0xe if (param.highNybble == 7) => SetTremoloWaveform(waveforms(param.lowNybble))
//    case class FineVolumeSlide(amount: Int) extends Effect // EAx/EBx
//    case class CutSampleAfterXTicks(ticks: Int) extends Effect // ECx
//    case class DelaySampleByTicks(ticks: Int) extends Effect // EDx
    case 0xf if (param <= 32) => SetTicksPerDivision(param)
    case 0xf if (param > 32) => SetBeatsPerMinute(param)
    case _ => Unimplemented(nr, param)
  }
}