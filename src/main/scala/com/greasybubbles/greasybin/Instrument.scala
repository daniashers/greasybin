package com.greasybubbles.greasybin

class Instrument(val name: String, val finetune: Int, val defaultVolume: Int, val repeatStart: Int, val repeatLength: Int, val digi: Seq[Byte]) {
  def size = digi.size
  val repeatEnd = repeatStart + repeatLength
  val isLooping = (repeatLength > 2) //some module have instruments with repeat length=2 but are still not looping...don't know why.

  def data(n: Int): Byte = if (n < 2) 0 else digi(n) //first two samples are overwritten

  val finetuneFactor = {
    val finetuneStep = Math.pow(2.0, 1.0/96.0) // eighth of a semitone
    val signedFinetune = if (finetune <= 7) finetune else finetune - 16 // (it is a signed nybble)
    Math.pow(finetuneStep, signedFinetune)
  }

  def calculateSampleIndex(n: Int): Int = {
    if (isLooping) {
      if (n < repeatStart) {
        n
      } else {
        repeatStart + ((n - repeatStart) % repeatLength)
      }
    } else {
      if (n < size) n else size-1 //hold last sample
    }
  }

  def calculateSample(n: Int): Byte = {
    val sampleIndex = calculateSampleIndex(n)
    if (sampleIndex >=0 && sampleIndex < digi.size) digi(sampleIndex) else 0
  }

  override def toString = "Instrument name: " + name + " size: %d, repeat offset: %d, repeat length: %d".format(size,repeatStart,repeatLength)
}

object Instrument {
  def NullInstrument = new Instrument("", finetune = 0, defaultVolume = 0, repeatStart = 0, repeatLength = 0, digi = IndexedSeq.empty[Byte])
}