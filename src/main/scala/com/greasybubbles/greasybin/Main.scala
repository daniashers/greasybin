package com.greasybubbles.greasybin

import java.io.{FileInputStream, File}

object Main {
  def main(args: Array[String]): Unit = {

    var fileContent: Array[Byte] = null
    try {
      val file: File = new File(args(0))
      val fileStream: FileInputStream = new FileInputStream(file)
      fileContent = new Array[Byte](file.length.toInt)
      fileStream.read(fileContent)
    } catch {
      case _: Throwable => println("Error reading file!")
    }
    val data: Stream[Byte] = fileContent.toList.toStream

    val module = ModuleLoader.loadFromStream(data).get

    module.instruments.foreach(println)

    def stateStream(start: ModPlayer): Stream[ModPlayer] = start #:: stateStream(start.next)
    val strm = stateStream(new ModPlayer(module, new Sequencer(module.song).copy(tick = -50), IndexedSeq.fill(4)(Channel.Initial), Synth.PAL))

    // set up audio output system
    val NrChannels = 2
    val BufferSize = 50
    val audioOut = new AudioPort(nrChannels = NrChannels, sampleRate = 44100, bytesPerSample = 1, bufferLengthMillis = BufferSize)

    strm.foreach (state => {

      if (state.seq.isNewDivision) print(state.seq.currentDivision + "\n")

      val output = state.mixedOutput

      var alreadySent = 0
      val ChunkSize = 1000
      while (alreadySent < output.length) {
        val remainingToSend = output.length - alreadySent
        val payloadSize = ChunkSize min remainingToSend
        val payload: Array[Byte] = Array.tabulate[Byte](payloadSize)(i => output(alreadySent+i))
        alreadySent += payloadSize
//        while (!audioOut.readyFor(state.syn.samplesPerTick * NrChannels)) {}
        while (!audioOut.readyFor(ChunkSize)) {}
        audioOut.playRaw(payload)
      }
    })
  }
}