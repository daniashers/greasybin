package com.greasybubbles.greasybin

import com.greasybubbles.binreader._
import com.greasybubbles.binreader.formats.NullPaddedString

object ModFormat {
  val sampleInfo = Composite[SampleInfo](List(
    Section("name", NullPaddedString(22)),
    Section("length", Word),
    Section("finetune", formats.Byte),
    Section("volume", formats.Byte),
    Section("repeatOffset", Word),
    Section("repeatLength", Word)
  ), m => SampleInfo(m.get("name").get.asInstanceOf[String], m.get("length").get.asInstanceOf[Int] * 2,
    m.get("finetune").get.asInstanceOf[Int], m.get("volume").get.asInstanceOf[Int],
    m.get("repeatOffset").get.asInstanceOf[Int] * 2, m.get("repeatLength").get.asInstanceOf[Int] * 2)
  )

  val modFmtPart = Sequence(List(
    Section("title", NullPaddedString(20)),
    Section("sampleDescription", Repeat(sampleInfo, 31)),
    Section("nrPatterns", formats.Byte),
    Section("ignore", formats.Byte),
    Section("patternSequence", Bytes(128)),
    Section("mk", NullPaddedString(4))
//    VariableSection("nrPatterns", n => Repeat(n, Section("patternData", ByteArray(128))),
  ))

}

case class SampleInfo(name: String, length: Int, finetune: Int, volume: Int, repeatOffset: Int, repeatLength: Int) {
  def addData(data: Seq[Byte]): Instrument = {
    //TODO check if length corresponds!
    new Instrument(name, finetune, volume, repeatOffset, repeatLength, data)
  }
}
