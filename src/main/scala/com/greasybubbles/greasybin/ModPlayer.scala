package com.greasybubbles.greasybin

case class ModPlayer(mod: Module, seq: Sequencer, chs: IndexedSeq[Channel], syn: Synth) {
  private lazy val (unmixedChannels, newChs) = chs.map(syn.playTick(_)).unzip

  val (ch1, ch2, ch3, ch4) = (unmixedChannels(0),unmixedChannels(1),unmixedChannels(2),unmixedChannels(3))

  def leftMix (s: Int): Byte = (0.30 * ch1(s) + 0.28 * ch2(s) + 0.32 * ch3(s) + 0.20 * ch4(s)).toByte
  def rightMix(s: Int): Byte = (0.20 * ch1(s) + 0.32 * ch2(s) + 0.28 * ch3(s) + 0.30 * ch4(s)).toByte
  lazy val mixedOutput: Array[Byte] = Array.tabulate(ch1.size*2)(i => if (i%2==0) leftMix(i/2) else rightMix(i/2))

  def next = {
    val newSequencer = seq.nextTick
    val ch1 = if (newSequencer.isNewDivision) {
      //if we have reached a new division, apply the next events to the channels
      val events = newSequencer.currentDivision.events
      IndexedSeq.tabulate(chs.size)(i => newChs(i).next.applyEvent(events(i), mod.instruments))
    } else {
      IndexedSeq.tabulate(chs.size)(i => newChs(i).next)
    }
    ModPlayer(mod, newSequencer, ch1, syn)
  }
}