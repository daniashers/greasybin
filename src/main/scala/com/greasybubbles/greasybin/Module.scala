package com.greasybubbles.greasybin

class Module(val title: String, val song: Song, val instruments: Seq[Instrument])
