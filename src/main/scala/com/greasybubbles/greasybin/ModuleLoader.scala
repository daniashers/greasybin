package com.greasybubbles.greasybin

import java.io.File

import com.greasybubbles.binreader.{SequenceOfSameType, Bytes, Repeat}

object ModuleLoader {
//  def loadFromFile(f: File): Module = {
//
//  }

  def loadFromStream(data: Stream[Byte]): Some[Module] = {

    if (!data.hasDefiniteSize) {
      None
    }

    val fileHeaderAndSampleInfo = ModFormat.modFmtPart.parse(data)
    //System.out.println(fileHeaderAndSampleInfo - "sampleDescription")

    //fileHeaderAndSampleInfo.get("sampleDescription").get.asInstanceOf[Vector[SampleInfo]].foreach(println)

    val nrPatterns = fileHeaderAndSampleInfo.get("nrPatterns").get.asInstanceOf[Int]

    val data2 = data.drop(ModFormat.modFmtPart.size)
    val patternsFormat = Repeat(Bytes(1024), nrPatterns)

    val patternData = patternsFormat.parse(data2).map(new Pattern(_))

    val samplesDescs = fileHeaderAndSampleInfo.get("sampleDescription").get.asInstanceOf[Vector[SampleInfo]]
    val samplesFormat = SequenceOfSameType(samplesDescs.map(x => Bytes(x.length)).toList)

    //cannot trust the number of patterns to be the number that is actually on the file.
    //To find the proper beginning of the samples the safest thing to do is count backwards from
    //the end of the file.

    val samplesStream = data.takeRight(samplesFormat.size)

    val samples = samplesFormat.parse(samplesStream)

    //println("Samples total length: " + samplesFormat.size)
    //println("Remaining file length: " + data3.size)

    //System.out.println("\n")

    val instruments: Seq[Instrument] = (0 to 30).map(i => samplesDescs(i).addData(samples(i)))

    //instruments.foreach(i => println(i.digi))

    val patternSequence = fileHeaderAndSampleInfo.get("patternSequence").get.asInstanceOf[Seq[Byte]].map(_.toInt)
    val sng = new Song(patternData, patternSequence)

    val title = fileHeaderAndSampleInfo.get("title").get.asInstanceOf[String]

    Some(new Module(title, sng, instruments))
  }
}
