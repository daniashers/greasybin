package com.greasybubbles.greasybin

object Music {
  val Semitone = Math.pow(2.0, 1.0/12.0)
  val EighthOfSemitone = Math.pow(2.0, 1.0/96.0)
}
