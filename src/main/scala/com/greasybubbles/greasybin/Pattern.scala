package com.greasybubbles.greasybin
import com.greasybubbles.greasybin.usefulint._

class Pattern(data: Seq[Byte]) {
  val Divisions = 64
  val Channels = 4
  val BytesPerEvent = 4

  def division(d: Int): Division = {
    Division(List.tabulate[Event](Channels)(event(d,_)))
  }

  def event(division: Int, channel: Int): Event = {
    val offset = (division % Divisions) * BytesPerEvent * Channels + (channel % Channels) * 4
    val byte0 = data(offset)
    val byte1 = data(offset + 1)
    val byte2 = data(offset + 2)
    val byte3 = data(offset + 3)
    Event.decode(byte0, byte1, byte2, byte3)
  }

  def dump: String = {
    List.tabulate(Divisions)(div => "%02X  ".format(div) + division(div).toString + "\n").foldLeft("")(_+_)
  }
}

case class Division(events: List[Event]) {
  override def toString: String = events.foldLeft("")(_ + _.toString + "  ")
}

case class NoteOn(sampleNr: Int, period: Int) {
  def isBlank = (sampleNr == 0 && period == 0)
}

case class Event(note: NoteOn, effect: Effect) {
  override def toString = {
    (if (note.sampleNr == 0) " -" else "%2X".format(note.sampleNr)) + " " +
      (if (note.period==0) "---" else "%03X".format(note.period)) + " %12s".format(effect.describe)
    // "???"
  }
}

object Event {
  def decode(byte0: Byte, byte1: Byte, byte2: Byte, byte3: Byte): Event = {
    val sampleNr: Int = (byte0.toUnsigned & 0xf0) | byte2.toUnsigned.highNybble
    val period = (byte0.lowNybble << 8) + byte1.toUnsigned
    val effectNumber = byte2.lowNybble
    val effectParameters = byte3.toUnsigned
    val fxHex = (effectNumber << 8) | effectParameters

    val fx = Effect.decode(effectNumber, effectParameters)

    Event(NoteOn(sampleNr, period), fx)
  }
}

