package com.greasybubbles.greasybin

case class Sequencer(song: Song, ticksPerDivision: Int = 6,
                  tablePos: Int = 0, divisionPos: Int = 0, tick: Int = 0, loopCounter: Int = 0) {

  override def toString = "Sequencer: '%s' " +
    "seq %2X, ptn %2X, div %02X, loop %d".format(tablePos,song.playlist(tablePos),divisionPos,loopCounter)


  val patternNr = song.playlist(tablePos)
  val currentPattern = song.patterns(patternNr)
  val currentDivision = currentPattern.division(divisionPos)
  val effects = currentDivision.events.map(_.effect)

  private def flowShiftingEffect: Option[Effect] = effects.reverse.find(_ match {
    case MarkLoopEnd(_) => true
    case PositionJump(_) => true
    case PatternBreak(_) => true
    case SetTicksPerDivision(_) => true
    case _ => false
  })

  private def findLoopStart(loopEnd: Int): Int = {
    effects.take(loopEnd)     // take only the first n effects up to the one before LoopEnd. We are not interested in any loop starts AFTER the loop end.
    .zipWithIndex             // take a note of the index as it is what we are interested in. Effect => (Effect, index)
    .reverse                  // we will search for a LoopStart, however we want the LAST seen, not the first, That's why we search the reversed list
    .find(_ == MarkLoopStart) // do the find. This returns an Option (None if not found)
    .map(_._2)                // take only the index (remember we are inside an Option)
    .getOrElse(0)             // return the index or, if none was found, return 0 (loop from the beginning)
  }

  private def normalNext: Sequencer = {
    if (divisionPos == 63) {
      this.copy(tablePos = tablePos + 1, divisionPos = 0)
    } else {
      this.copy(divisionPos = divisionPos + 1)
    }
  }

  def nextTick = {
    if (tick < ticksPerDivision-1) this.copy(tick = tick + 1)
    else this.copy(tick = 0).nextDivision
  }

  def nextDivision: Sequencer = {
    // check for the following effects:
    // E6x: loop
    // Bxx: table jump
    // Dxx: pattern break
    val fx = flowShiftingEffect
    if (fx == None) normalNext
    else flowShiftingEffect.get match {
      case MarkLoopEnd(loops) => if (loopCounter < loops) {
        this.copy(divisionPos = findLoopStart(divisionPos), loopCounter = loopCounter + 1)
      } else {
        normalNext.copy(loopCounter = 0) //TODO test this and also figure out a way to make it more elegant!
      }
      case PositionJump(newPos) => this.copy(tablePos = newPos, divisionPos = 0)
      case PatternBreak(resumePos) => this.copy(tablePos = tablePos + 1, divisionPos = resumePos)
      case SetTicksPerDivision(tpd) => this.copy(ticksPerDivision = tpd).normalNext //TODO wrong! because it only works AFTER the division
      case _ => normalNext
    }
  }

  def isNewDivision: Boolean = tick == 0
}