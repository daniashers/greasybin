package com.greasybubbles.greasybin

class Synth(clock: Double, timer: Double, sampleRate: Int) {
  val cyclesPerSample = clock/sampleRate
  val samplesPerTick: Int = Math.round(timer * sampleRate).toInt

  def playTick(ch: Channel) = play(ch)(samplesPerTick)

  def play(ch: Channel)(nrSamples: Int): (IndexedSeq[Byte], Channel) = {

    def inSampNr(outSampNr: Int, period: Int): Int = {
      val cyclesElapsed: Double = cyclesPerSample * outSampNr
      val inSampsElapsed: Double = ch.sampleIndex + (ch.periodCounter + cyclesElapsed) / period
      inSampsElapsed.toInt
    }

    val instr = ch.instrument
    val volume = ch.volume
    val outSamples = Vector.tabulate[Byte](nrSamples)(i => {
      Math.round((volume.toFloat / 64.0) * instr.calculateSample(inSampNr(i, ch.period))).toByte
    })
    val newCounter = if (ch.period == 0) 0 else (ch.periodCounter + cyclesPerSample * nrSamples).toInt % ch.period
    (
      outSamples,
      ch.copy(
        sampleIndex = instr.calculateSampleIndex(inSampNr(nrSamples, ch.period)),
        periodCounter = newCounter
      )
    )
  }
}

object Synth {
  def PAL  = new Synth(3546895, 1.0/50.0, 44100)
  def NTSC = new Synth(3579545, 1.0/60.0, 44100)
}

