package com.greasybubbles.greasybin

package object usefulint {

  implicit class UsefulInt(val v: Int) extends AnyVal {
    def highNybble = (v >> 4) & 0x0f

    def lowNybble = v & 0x0f
    def clamp(min: Int, max: Int) = v.max(min).min(max)
  }

  implicit class UsefulByte(val b: Byte) extends AnyVal {
    def toUnsigned: Int = if (b < 0) b.toInt + 256 else b.toInt
  }
}