package com.greasybubbles.greasybin

trait Waveform {
  def get(index: Int): Double
}

case class Sine(retrigger: Boolean = false) extends Waveform {
  def get(index: Int) = Math.sin((index % 64).toDouble / 64.0 * 2 * Math.PI)
}
case class SawDown(retrigger: Boolean = false) extends Waveform {
  def get(index: Int) = (1.0 - (index % 64).toDouble / 32.0)
}
case class Square(retrigger: Boolean = false) extends Waveform {
  def get(index: Int) = if ((index % 64) < 32) 1.0 else -1.0
}
case class Random(retrigger: Boolean = false) extends Waveform {
  def get(index: Int) = Math.random() * 2 - 1.0
}
