package com.greasybubbles.greasybin

import com.greasybubbles.binreader
import com.greasybubbles.binreader._
import com.greasybubbles.binreader.formats.NullPaddedString
import org.scalatest.{FlatSpec, Matchers}

class Test1 extends FlatSpec with Matchers {

  "The byte parser" should "return the correct value" in {
    val fmt = formats.Byte
    val stream: Stream[Byte] = List[Byte](0x15, 0x22, 0x00).toStream
    fmt.parse(stream) should be (0x15)
  }

  it should "Z" in {
    1 should be (1)
  }

  "The word parser" should "return the correct value" in {
    val fmt = Word
    val stream: Stream[Byte] = List[Byte](0x15, 0x22, 0x00).toStream
    fmt.parse(stream) should be (5410)
  }

  "The string parser" should "return the correct value" in {
    val fmt = NullPaddedString(4)
    val stream: Stream[Byte] = List[Byte](0x41, 0x42, 0x00, 0x00).toStream
    fmt.parse(stream) should be ("AB")
  }

  "A sequence parser" should "work properly" in {
    val fmt = Sequence(List[Section](Section("number",formats.Byte), Section("letter",NullPaddedString(1))))
    val stream: Stream[Byte] = List[Byte](0x32, 0x41, 0x00, 0x00).toStream
    val map = fmt.parse(stream)
    map.get("number") should be (Some(0x32))
    map.get("letter") should be (Some("A"))
    map.get("invalidKey") should be (None)
  }

  "A repeat parser" should "work properly" in {
    val fmt = Repeat(formats.Byte, 3)
    val stream: Stream[Byte] = List[Byte](0x10, 0x20, 0x30, 0x00).toStream
    val result = fmt.parse(stream)
    result should be (Vector(0x10,0x20,0x30))
  }


}
